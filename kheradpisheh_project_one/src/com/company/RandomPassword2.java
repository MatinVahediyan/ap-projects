package com.company ;

import com.sun.source.tree.BreakTree;

import java.util.ArrayList ;
import java.util.Scanner ;
import java.lang.Math ;

public class RandomPassword2
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int code_length = input.nextInt () ;
        char [] code_array = new char [code_length] ;

        code_array [0] = RandomChar () ;
        code_array [1] = RandomDigit () ;

        for (int index = 2 ; index < code_length ; index ++)
        {
            code_array [index] = RandomAll () ;
        }

        PrintCode (code_array) ;
    }

    public static char RandomChar ()
    {
        int random = (int) (Math.random () * 7) ;

        if      (random % 6 == 0)  return (char) 33 ;
        else if (random % 6 == 1)  return (char) 37 ;
        else if (random % 6 == 2)  return (char) 35 ;
        else if (random % 6 == 3)  return (char) 36 ;
        else if (random % 6 == 4)  return (char) 38 ;
        else                       return (char) 64 ;
    }

    public static char RandomDigit ()
    {
        int random = (int) (Math.random () * 11) ;

        if      (random % 10 == 0)  return (char) 48 ;
        else if (random % 10 == 1)  return (char) 49 ;
        else if (random % 10 == 2)  return (char) 50 ;
        else if (random % 10 == 3)  return (char) 51 ;
        else if (random % 10 == 4)  return (char) 52 ;
        else if (random % 10 == 5)  return (char) 53 ;
        else if (random % 10 == 6)  return (char) 54 ;
        else if (random % 10 == 7)  return (char) 55 ;
        else if (random % 10 == 8)  return (char) 56 ;
        else                        return (char) 57 ;
    }

    public static char RandomAll ()
    {
        int random = (int) (Math.random () * 43) ;

        if      (random % 43 == 0)  return (char) 33 ;
        else if (random % 43 == 1)  return (char) 37 ;
        else if (random % 43 == 2)  return (char) 35 ;
        else if (random % 43 == 3)  return (char) 36 ;
        else if (random % 43 == 4)  return (char) 38 ;
        else if (random % 43 == 5)  return (char) 64 ;
        else if (random % 43 == 6)  return (char) 48 ;
        else if (random % 43 == 7)  return (char) 49 ;
        else if (random % 43 == 8)  return (char) 50 ;
        else if (random % 43 == 9)  return (char) 51 ;
        else if (random % 43 == 10)  return (char) 52 ;
        else if (random % 43 == 11)  return (char) 53 ;
        else if (random % 43 == 12)  return (char) 54 ;
        else if (random % 43 == 13)  return (char) 55 ;
        else if (random % 43 == 14)  return (char) 56 ;
        else if (random % 43 == 15)  return (char) 57 ;
        else if (random % 43 == 16)  return (char) 97 ;
        else if (random % 43 == 17)  return (char) 98 ;
        else if (random % 43 == 18)  return (char) 99 ;
        else if (random % 43 == 19)  return (char) 100 ;
        else if (random % 43 == 20)  return (char) 101 ;
        else if (random % 43 == 21)  return (char) 102 ;
        else if (random % 43 == 22)  return (char) 103 ;
        else if (random % 43 == 23)  return (char) 104 ;
        else if (random % 43 == 24)  return (char) 105 ;
        else if (random % 43 == 25)  return (char) 106 ;
        else if (random % 43 == 26)  return (char) 107 ;
        else if (random % 43 == 27)  return (char) 108 ;
        else if (random % 43 == 28)  return (char) 109 ;
        else if (random % 43 == 29)  return (char) 110 ;
        else if (random % 43 == 30)  return (char) 111 ;
        else if (random % 43 == 31)  return (char) 112 ;
        else if (random % 43 == 32)  return (char) 113 ;
        else if (random % 43 == 33)  return (char) 114 ;
        else if (random % 43 == 34)  return (char) 115 ;
        else if (random % 43 == 35)  return (char) 116 ;
        else if (random % 43 == 36)  return (char) 117 ;
        else if (random % 43 == 37)  return (char) 118 ;
        else if (random % 43 == 38)  return (char) 119 ;
        else if (random % 43 == 39)  return (char) 120 ;
        else if (random % 43 == 40)  return (char) 121 ;
        else                         return (char) 122 ;
    }

    public static void PrintCode (char [] array)
    {
        ArrayList <Integer> used_index = new ArrayList <Integer> () ;

        int index = 0 ;

        while (true)
        {
            int random = (int) (Math.random () * array.length) ;

            if (used_index.contains (random))
            {
                continue ;
            }

            else
            {
                System.out.print (array [random]) ;
                used_index.add (random) ;
                index ++ ;
            }

            if (index == array.length) return ;
        }
    }
}