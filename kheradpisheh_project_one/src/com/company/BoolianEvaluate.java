package com.company ;

import java.util.Scanner ;
import java.lang.Character ;

public class BoolianEvaluate
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;
        int p = input.nextInt () ;
        String x = input.next () ;
        int q = input.nextInt () ;

        if (x == "a")
        {
            if (p == 1 || q == 1)
            {
                System.out.println ("Result is true !") ;
                return ;
            }

            else
            {
                System.out.println ("Result is false !") ;
                return ;
            }
        }

        if (x == "o")
        {
            if (p == 1 && q == 1)
            {
                System.out.println ("Result is true !") ;
                return ;
            }

            else
            {
                System.out.println ("Result is false !") ;
                return ;
            }
        }

        if (x == "x")
        {
            if ((p == 1 && q == 0) || (p == 0 && q == 1))
            {
                System.out.println ("Result is true !") ;
                return ;
            }

            else
            {
                System.out.println () ;
                return ;
            }
        }

        if (x == "t")
        {
            if (p == 1 && q == 0)
            {
                System.out.println ("Result is false !") ;
                return ;
            }

            else
            {
                System.out.println ("Result is false !") ;
                return ;
            }
        }
    }
}
