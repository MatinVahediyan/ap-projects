package com.company ;

import java.util.Scanner ;

public class RevArray
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int array_index = input.nextInt () ;
        int [] input_array = new int [array_index] ;
        int [] output_array = new int [array_index] ;

        for (int index = 0 ; index < array_index ; index ++)
        {
            input_array [index] = input.nextInt () ;
        }

        int index2 = 0 ;

        for (int index1 = array_index - 1 ; index1 >= 0 ; index1 --)
        {
            output_array [index2] = input_array [index1] ;
            index2 ++ ;
        }

        for (int index = 0 ; index < array_index ; index ++)
        {
            System.out.print (output_array [index] + " ") ;
        }
    }
}
