package com.company ;

import java.util.Scanner ;

public class MatrixProduct
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int row1 = input.nextInt () ;
        int culumn1 = input.nextInt () ;

        int [][] matrix1 = new int [row1][] ;

        for (int index1 = 0 ; index1 < row1 ; index1 ++)
        {
            for (int index2 = 0 ; index2 < culumn1 ; index2 ++)
            {
                matrix1 [index1][index2] = input.nextInt () ;
            }
        }

        int row2 = input.nextInt () ;

        int [][] matrix2 = new int [row2][] ;

        for (int index1 = 0 ; index1 < row2 ; index1 ++)
        {
            for (int index2 = 0 ; index2 < culumn1 ; index2 ++)
            {
                matrix2 [index1][index2] = input.nextInt () ;
            }
        }

        int temp = 0 ;
        int counter1 = 0 , counter2 = 0 ;
        int [][] matrix3 = new int [row1][] ;

        int sum = 0 ;

        for (int index1 = 0 ; index1 < row1 ; index1 ++)
        {
            for (int index2 = 0 ; index2 < row2 ; index2 ++)
            {
                for (int index3 = 0 ; index3 < culumn1 ; index3++)
                {
                    sum += matrix1 [index1][temp] * matrix2 [index3][index2] ;
                    temp ++ ;
                }

                matrix3 [counter1][counter2] = sum ;

                counter2 ++ ;
                temp = 0 ;
                sum = 0 ;
            }

            counter1 ++ ;
            counter2 = 0 ;
        }

        for (int index1 = 0 ; index1 < row1 ; index1 ++)
        {
            for (int index2 = 0 ; index2 < culumn1 ; index2 ++)
            {
                System.out.print (matrix3 [index1][index2]) ;
            }

            System.out.println () ;
        }
    }
}