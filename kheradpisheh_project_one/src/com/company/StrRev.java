package com.company ;

import java.util.Scanner ;
import java.lang.String ;

public class StrRev
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        String string_for_revers ;
        string_for_revers = input.next () ;

        System.out.println (Swap (string_for_revers)) ;
    }

    public static String Swap (String str)
    {
        if (str.isEmpty () || str.length () == 1)
        {
            return str ;
        }

        StringBuilder sample = new StringBuilder (str) ;
        int counter2 = str.length () - 1 ;

        for (int counter1 = 0 ; counter1 <= (str.length () - 1) / 2 ; counter1 ++)
        {
            char char1 = sample.charAt (counter1) ;
            char char2 = sample.charAt (counter2) ;

            sample.setCharAt (counter2 , char1) ;
            sample.setCharAt (counter1 , char2) ;

            counter2 -- ;
        }

        return sample.toString () ;
    }
}
