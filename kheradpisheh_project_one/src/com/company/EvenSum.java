package com.company ;

import java.util.Scanner ;

public class EvenSum
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int array_length ;
        array_length = input.nextInt () ;

        int [] array = new int [array_length] ;

        for (int index = 0 ; index < array_length ; index ++)
        {
            array [index] = input.nextInt () ;
        }

        int sumation = 0 ;

        for (int index = 0 ; index < array_length ; index += 2)
        {
            sumation += array [index] ;
        }

        System.out.println (sumation) ;
    }
}
