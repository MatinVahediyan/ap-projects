package com.company;

public class Main
{
    public static void main (String [] args)
    {
        int year = 0 ;
        double money = 10000.00 ;
        final double PERCENTAGE_INTEREST = 0.05 ;

        for ( ; ; year ++)
        {
            money += money * PERCENTAGE_INTEREST ;

            if (money >= 20000)
            {
                System.out.print(year);
                break;
            }
        }

        System.exit (0) ;
    }
}