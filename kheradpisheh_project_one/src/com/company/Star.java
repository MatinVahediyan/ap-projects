package com.company ;

import java.util.Scanner ;

public class Star
{

    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number_of_rows ;
        number_of_rows = input.nextInt () ;

        PrintStar (number_of_rows) ;
    }

    public static void PrintStar (int rows)
    {
        if (rows < 1) return ;

        PrintStar (rows - 1) ;

        for (int print_star = 1 ; print_star <= rows ; print_star ++)
        {
            System.out.print ("*") ;
        }

        System.out.println () ;
    }
}