package com.company ;

import java.util.Scanner ;

public class Guess
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        final int PRIVATE_CODE = 1250 ;

        System.out.println ("Print your guesses (Print 0 to exit) :") ;

        while (true)
        {
            int guess_code ;
            guess_code = input.nextInt () ;

            if (guess_code == PRIVATE_CODE)
            {
                System.out.println ("Your guess is right !!!") ;
                return ;
            }

            else if (guess_code == 0)
            {
                return ;
            }

            String result = (guess_code >= PRIVATE_CODE - 100 && guess_code <= PRIVATE_CODE + 100) ? "Your guess is too high" : "Your guess is too low" ;
            System.out.println (result) ;
        }
    }
}