package com.company ;

import java.util.Scanner ;

public class Palindrom
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        String string ;
        string = input.next () ;

        if (string.isEmpty () || string.length () == 1)
        {
            System.out.println ("Yes it is palindrom !!!") ;
            return ;
        }

        int index_left = 0 ;
        int index_right = string.length () - 1 ;

        CheckPalindrom (string , index_left , index_right) ;

        System.out.println ("Yes it is palindrom !!!") ;
    }

    public static void CheckPalindrom (String str , int index1 , int index2)
    {
        if (index1 > (str.length () - 1) / 2)  return ;

        CheckPalindrom (str , index1 + 1 , index2 - 1) ;

        boolean result = (str.charAt (index1) == str.charAt (index2)) ? true : false ;

        if (!result)
        {
            System.out.println ("No it is lopsided !!!") ;
            System.exit (0) ;
        }
    }
}
