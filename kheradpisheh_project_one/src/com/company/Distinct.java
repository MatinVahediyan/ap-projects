package com.company;

import java.util.Scanner ;

public class Distinct
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        String string ;
        string = input.next () ;

        if (string.isEmpty ())
        {
            System.out.println (0) ;
            return ;
        }

        int sumation = 0 ;

        for (int index = 0 ; index < string.length () - 2 ; index ++)
        {
            if (string.charAt (index) == string.charAt (index + 1))
            {
                sumation ++ ;
            }
        }

        System.out.println (sumation) ;
    }
}