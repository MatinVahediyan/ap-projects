package com.company ;

import java.util.ArrayList ;
import java.util.Arrays ;
import java.util.List;
import java.util.Scanner ;

public class ArrayToList
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int index1 = input.nextInt () ;
        int index2 = input.nextInt () ;

        String [][] array = new String [index1][] ;

        for (int row = 0 ; row < index1 ; row ++)
        {
            for (int culum = 0 ; culum < index2 ; culum ++)
            {
                array [row][culum] = input.next () ;
            }
        }

        ArrayList <String> [] arrylist = new ArrayList [index1] ;

        for (int row = 0 ; row < index1 ; row ++)
        {
            arrylist [row] = new ArrayList () ;
        }

        for (int row = 0 ; row < index1 ; row ++)
        {
            for (int culum = 0 ; culum < index2 ; culum ++)
            {
                arrylist[row].add(array[row][culum]) ;
            }
        }
    }
}
