package com.company ;

import java.util.Scanner ;

public class BoxString
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        String str = input.nextLine () ;

        PrintBox (str.length ()) ;
        System.out.println (str) ;
        PrintBox (str.length ()) ;
    }

    public static void PrintBox (int box_length)
    {
        for (int index = 0 ; index <box_length ; index ++)
        {
            System.out.print ("-") ;
        }
        System.out.println () ;
    }
}
