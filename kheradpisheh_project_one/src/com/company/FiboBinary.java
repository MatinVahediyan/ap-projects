package com.company ;

import java.util.Scanner ;
import java.lang.Math ;

public class FiboBinary
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number ;
        number = input.nextInt () ;

        boolean flag = false ;

        for (int counter = 1 ; counter <= number ; counter ++)
        {
            if (Fib (counter) + Bin (Fib (counter)) == number)
            {
                flag = true ;
                break ;
            }
        }

        if (flag)
        {
            System.out.println ("This number is fibobinary !!") ;
        }
        else
        {
            System.out.println ("This number is'nt fibobinary !!") ;
        }
    }

    public static int Fib (int n)
    {
        int result ;

        result = (n == 1 || n == 2) ? 1 : Fib (n - 1) + Fib (n - 2) ;

        return result ;
    }

    public static int Bin (int n)
    {
        if (n == 1) return 1 ;

        int counter = 0 ;
        int binary = 0 ;

        for ( ; n > 1 ; n /= 2)
        {
            int remain = n % 2 ;
            binary += remain * Math.pow (10 , counter) ;
            counter ++ ;
        }

        int result = 0 ;

        for ( ; binary >= 1 ; binary /= 10)
        {
            if (binary % 10 == 1)
            {
                result ++ ;
            }
        }

        return result ;
    }
}
