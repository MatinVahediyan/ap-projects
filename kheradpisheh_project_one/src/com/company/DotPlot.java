package com.company ;

import java.util.Scanner ;

public class DotPlot
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        String str1 ;
        String str2 ;

        System.out.println ("Please enter two string :") ;

        str1 = input.next () ;
        str2 = input.next () ;

        for (int index_str1 = 0 ; index_str1 < str1.length () ; index_str1 ++)
        {
            System.out.print (" " + str1.charAt (index_str1)) ;
        }

        System.out.println () ;

        for (int index_str2 = 0 ; index_str2 < str2.length () ; index_str2 ++)
        {
            System.out.print (str2.charAt (index_str2)) ;

            for (int index_str1 = 0 ; index_str1 < str1.length () ; index_str1 ++)
            {
                if (str1.charAt (index_str1) == str2.charAt (index_str2))
                {
                    System.out.print ("*") ;
                }

                else
                {
                    System.out.print (" ") ;
                }
            }

            System.out.println () ;
        }
    }
}
