package com.company ;

import java.util.Scanner ;

public class PrimeFactor
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number = input.nextInt () ;
        int [] prim = new int [number] ;

        int index = 0 ;

        for (int sample = 2 ; sample <= number ; sample ++)
        {
            boolean check = true ;

            for (int temp = 2 ; temp <= sample / 2 ; temp ++)
            {
                if (sample % temp == 0)
                {
                    check = false ;
                    break ;
                }
            }

            if (check)
            {
                prim [index] = sample ;
                index ++ ;
            }
        }

        for (int counter = 0 ; prim [counter] != 0 ; counter ++)
        {
            CheckFactor (number , prim [counter]) ;
        }
    }

    public static void CheckFactor (int number , int prim)
    {
        if (number % prim == 0)
        {
            System.out.print (prim + " ") ;
        }
    }
}
