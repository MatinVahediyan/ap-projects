package com.company ;

import java.util.Scanner ;

public class Brackets
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number = input.nextInt () ;

        Print (2 * number , 2 * number) ;
    }

    public static void Print (int n , int x)
    {
        if (n < 1) return ;

        Print (n - 1 , x) ;

        if (n <= x / 2)
        {
            System.out.print ("[") ;
        }
        else if (n > x / 2)
        {
            System.out.print ("]") ;
        }
    }
}
