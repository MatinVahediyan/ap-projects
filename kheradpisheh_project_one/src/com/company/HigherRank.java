package com.company;

import java.util.Scanner ;
import java.lang.Math ;

public class HigherRank
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int score1 , score2 ;
        String name1 , name2 ;

        System.out.println ("Please print name of student and them scores :") ;

        System.out.print ("name 1 : ") ;
        name1 = input.next () ;

        System.out.print ("your score : ") ;
        score1 = input.nextInt () ;

        System.out.print ("name 2 : ") ;
        name2 = input.next () ;

        System.out.print ("your score : ") ;
        score2 = input.nextInt () ;

        if (score1 > score2)
        {
            System.out.println (name1) ;
            return ;
        }

        else if (score2 > score1)
        {
            System.out.println (name2) ;
        }

        else
        {
            for (int counter = 0 ; counter <= Math.min (name1.length () , name2.length ()) ; counter ++)
            {
                if ((int) name1.charAt (counter) == (int) name2.charAt (counter)) continue ;

                else if ((int) name1.charAt (counter) > (int) name2.charAt (counter))
                {
                    System.out.println (name2) ;
                    return ;
                }

                else
                {
                    System.out.println (name1) ;
                    return ;
                }
            }
        }
    }
}
