package com.company ;

import java.util.* ;
import java.lang.Math ;

public class RandomPassword
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int code_length = input.nextInt () ;

        for (int index = 0 ; index <code_length ; index ++)
        {
            int random_integer = (int) (Math.random () * 26 + 97) ;
            System.out.print ((char) random_integer) ;
        }
    }
}