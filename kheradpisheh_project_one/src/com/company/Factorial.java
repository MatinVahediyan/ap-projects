package com.company;

import java.util.Scanner ;

public class Factorial
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number ;
        number = input.nextInt () ;

        System.out.print (Implement (number)) ;
    }

    public static int Implement (int value)
    {
        if (value == 1 || value == 0) return 1 ;

        int fact = Implement (value - 1) * value ;
        return fact ;
    }
}
