package com.company ;

import java.util.Scanner ;

public class Fibonachy
{
    public static void main (String [] args)
    {
        Scanner input = new Scanner (System.in) ;

        int number ;
        number = input.nextInt () ;

        System.out.println (Fib (number)) ;
    }

    public static int Fib (int n)
    {
        int result ;

        result = (n == 1 || n == 2) ? 1 : Fib (n - 1) + Fib (n - 2) ;

        return result ;
    }
}
