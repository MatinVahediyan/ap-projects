package com.company ;

import java.util.Scanner ;

public class MatrixProdact2
{
    public static void main (String [] args)
    {
        int num1 ;
        int num2 ;
        int num3 ;

        int [][] matrix1 ;
        int [][] matrix2 ;
        int [][] matrix3 ;

        matrix1 = new int [10][10] ;
        matrix2 = new int [10][10] ;
        matrix3 = new int [10][10] ;

        Scanner input = new Scanner (System.in) ;

        num1 = input.nextInt () ;
        num2 = input.nextInt () ;
        num3 = input.nextInt () ;

        for (int i = 0 ; i < num1 ; i++)
        {
            for (int j = 0 ; j < num2 ; j++)
            {
                matrix1 [i][j] = input.nextInt () ;
            }
        }

        for (int i = 0 ; i < num2 ; i++)
        {
            for (int j = 0 ; j < num3 ; j++)
            {
                matrix2 [i][j] = input.nextInt () ;
            }
        }

        int p = 0 ;
        int q = 0 ;
        int w = 0 ;
        int sum = 0 ;

        for (int i = 0 ; i < num1 ; i++)
        {
            for (int j = 0 ; j < num3 ; j++)
            {
                for (int z = 0 ; z < num2 ; z++)
                {
                    sum += matrix1 [i][w] * matrix2 [z][j] ;
                    w ++ ;
                }

                matrix3 [p][q] = sum ;

                q ++ ;
                w = 0 ;
                sum = 0 ;
            }

            p ++ ;
            q = 0 ;
        }

        print_matrix (matrix3 , num1 , num3) ;

        input.close () ;

        System.exit (0) ;
    }

    static void print_matrix (int [][] matrix3 , int num1 , int num3)
    {
        for (int i = 0 ; i < num1 ; i++)
        {
            for (int j = 0 ; j < num3 ; j++)
            {
                System.out.print (matrix3 [i][j]) ;
                System.out.print (" ") ;
            }

            System.out.println () ;
        }
    }
}
